//Logeo de la plicacion
const logger = require('log4js').getLogger('BBBDD');
const { response } = require('express');

//Modelo
const Database = require( '../models/database' );


const listar = async ( req, res = response ) => {

  // const desde   = Number(req.query.desde) || 0;
  // const limite  = Number(req.query.limite) || 5;


  const [ database, total ] = await Promise.all([
    Database
      .find({ estado: true })
      // .skip( desde )
      // .limit( limite )
      .populate( 'usuario', 'nombre' ),

    Database.countDocuments({ estado: true })
  ]);


  res.json({
    ok: true,
    total,
    database
  });



}


const detalle = async ( req, res = response ) => {

  const _id = req.params.id;

  const [ database, total ] = await Promise.all([
    Database
      .findById( { _id } )
      .populate( 'usuario', 'nombre' ),

    Database.countDocuments({ estado: true })
  ]);


  res.json({
    ok: true,
    total,
    database
  });



}


const crear = async ( req, res = response ) => {


  const database = new Database( 
    {
      creador: req.uid,
      ...req.body
    } 
  );

  try {

    const existeDB = await Database.find( 
      { 
        "host": req.body.host ,
        "servicio": req.body.servicio 
      } 
    ).exec();

    if ( existeDB.length ) {
      
      return res.status( 500 ).json({
        ok: false,
        msg: `El host y servicio '${ req.body.host } / ${ req.body.servicio }' en el entorno ${ req.body.entorno } ya existe en el entorno ${ existeDB[0].entorno } del ambiente ${ existeDB[0].ambiente }`
      });
    }

    await database.save();
    
    res.json({
      ok: true,
      database
    });

  } catch (error) {
    
    logger.error( `Database - controller - ${ error }` );

    res.status( 500 ).json({
      ok: false,
      msg: 'Hable con el administrador'
    });

  }


}


const actualizar = async ( req, res = response ) => {

  res.json({
    ok: true,
    msg: 'Actualizar TNS de BBDD'
  });


}

const eliminar = async ( req, res = response ) => {

  const _id = req.params.id;

  try {
    
    const eliminado = await Database.findByIdAndDelete( _id );

    console.log( eliminado );

    res.json({
      ok: true,
      database: eliminado
    });


  } catch (error) {
    logger.error( `Controller - eliminar - ${ error }` );

    res.status( 500 ).json({
      ok: false,
      msg: 'Hable con el administrador'
    });

  }

}

const deshabilitar = async ( req, res = response ) => {

  const _id = req.params.id;


  try {
    
    const databaseDB = await Database.findById( {_id } ); 
    
    if ( !databaseDB ) {
      return res.status( 404 ).json({
        ok: false,
        msg: `No existe BBDD con ID ${ _id }`
      });
    }

    databaseDB.estado = false;

    const databaseUP = await Database.findByIdAndUpdate( { _id }, databaseDB, { new: true } );


    res.json({
      ok: true,
      database: databaseUP
    });

  } catch (error) {
    
    logger.error( `Database - controller - ${ error }` );

    res.status( 500 ).json({
      ok: false,
      msg: 'Hable con el administrador'
    });

  }


}

module.exports = {
  listar,
  crear,
  actualizar,
  eliminar,
  detalle,
}