/**********************************************************************************
 * 
 * Importación de paquetes
 * 
 */
const logger = require('log4js').getLogger('auth');
const { response } = require( 'express' );
const bcrypt = require( 'bcryptjs' );
const generator = require('generate-password');

/**********************************************************************************
 * 
 * Modelos
 * 
 */
const Usuario = require( '../models/usuario' );

/**********************************************************************************
 * 
 * Helpers
 * 
 */
const { generarJWT } = require( '../helpers/jwt' );
const { enviarEmail } = require( '../helpers/sengGrid' );


/*****************************************************************************
 * 
 * Templates
 * 
 */
 const { templateRestorePassword } = require( '../templates/email.template' );



const login = async ( req, res = response ) => {

  const { email, password } = req.body;

  try {

    // Verificar Email
    const usuarioDB = await Usuario.findOne( {email} );

    if ( !usuarioDB ) {
      return res.status( 404 ).json({
        ok: false,
        msg: 'Usuario o contraseña incorrectos'
      });
    }

    // Verificar contraseña
    const validadPasswprd  = bcrypt.compareSync( password, usuarioDB.password );

    if ( !validadPasswprd ) {

      logger.warn( `Contraseña incorresta para el usuario ${ email }` );

      return res.status( 404 ).json({
        ok: false,
        msg: 'Usuario o contraseña incorrectos'
      });
    }

    // Generar Token JWT
    const token = await generarJWT( usuarioDB.id );

    res.json({
      ok: true,
      token
    });

  } catch (error) {
    
  }

}

const renewToken = async (req, res = response) => {

  const uid = req.uid;

  // Generar el TOKEN - JWT
  const token = await generarJWT( uid );

  // Retornar usuario
  const usuarioDB = await Usuario.findById( uid ); 


  res.json({
      ok: true,
      token,
      usuario: usuarioDB
  });

}

const restorePassword = async ( req, res = response ) => {

  const email  = req.params.email;

  try {
    // Verificar Email
    logger.debug( `Buscando en la BBDD el usuario ${ email }` );
    const usuario = await Usuario.findOne( {email} );


    if ( !usuario ) {
      logger.warn
      return res.json({
        ok: false,
        msg: `El mail ${ email } no existe en la BBDD`
      });
    }

    // Genero una contraseña aleatoria
    logger.debug( `Generando la nueva contraseña`);
    const password = generator.generate({
      length: 10,
      numbers: true
    });


    //------------------------------------------------------------
    //encriptar contraseña
    logger.debug( 'Encriptando la nueva contraseña' );
    const salt = bcrypt.genSaltSync();
    usuario.password = bcrypt.hashSync( password, salt );
    
    logger.debug( 'Guardando la nueva contraseña' );
    usuario.save();

    //------------------------------------------------------------ 
    
    /**********************************************************************************
     * 
     * Si el mail existe lo envio
     * 
     */
     logger.debug( 'Enviando la contraseña por email' );
    const msg = templateRestorePassword( password );
    await enviarEmail( email, msg );


  } catch (error) {
    
    logger.error( `Error en restorePassword: ${ error }` );

    res.status( 500 ).json({
      ok: false,
      msg: `Comuniquese con el administrador`
    });

  }

}

module.exports = {
  login,
  renewToken,
  restorePassword
}