//Logeo de la plicacion
const logger = require('log4js').getLogger('accesos-rapidos');
const { response } = require('express');


// Modelo
const AccesosRapidos = require( '../models/accesos-rapidos' );



const listar = async ( req, res = response ) => {

  const accesos = await AccesosRapidos.find( {} )
                                        .populate( 'creador', 'nombre' );



  res.json({
    ok: true,
    accesos
  });

}

const detalle = async ( req, res = response ) => {

  const id = req.params.id;

  if( !id ){
    res.status(500).json({
      ok: false,
      msg: 'Se reguiere el ID'
    });

    return;
  }


  const acceso = await AccesosRapidos.find( { _id: id } )
                                        .populate( 'creador', 'nombre' );


  res.json({
    ok: true,
    acceso
  });

}


const crear = async ( req, res = response ) => {

  const acceso = new AccesosRapidos({
    creador: req.uid,
    ...req.body
  });

  try {

    await acceso.save();

    res.json({
      ok: true,
      acceso
    });
    
  } catch (error) {
    logger.error( `Database - controller - ${ error }` );

    res.status( 500 ).json({
      ok: false,
      msg: 'Hable con el administrador'
    });
  }

}


const eliminar = async ( req, res = response ) => {

  const id = req.params.id;

  try {

    await AccesosRapidos.findByIdAndDelete( id );
    
    res.json({
      ok: true,
      msg: `Se elimino correctamente el acceso`
    });


  } catch (error) {
    logger.error( `eliminar - ${ error }` );
  
      res.status( 500 ).json({
        ok: false,
        msg: 'No se pudo eliminar el acceso'
      });
  }

}


module.exports = {
  listar,
  crear,
  detalle,
  eliminar
}