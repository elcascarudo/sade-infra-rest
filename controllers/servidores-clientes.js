//Logeo de la plicacion
const logger = require('log4js').getLogger('usuarios');
const { response } = require('express');


// Modelo
const ServidoresClientes = require( '../models/servidores-clientes' );



const listar = async ( req, res = response ) => {

  const servidores = await ServidoresClientes.find( {} )
                                              .populate( 'creador', 'nombre' );



  res.json({
    ok: true,
    servidores
  });

}


const crear = async ( req, res = response ) => {

  const servidor = new ServidoresClientes({
    creador: req.uid,
    ...req.body
  });

  try {

    await servidor.save();

    res.json({
      ok: true,
      servidor
    });
    
  } catch (error) {
    logger.error( `Database - controller - ${ error }` );

    res.status( 500 ).json({
      ok: false,
      msg: 'Hable con el administrador'
    });
  }

}


module.exports = {
  listar,
  crear
}