//Logeo de la plicacion
const logger = require('log4js').getLogger('dbpass');

const { response } = require('express');

const Usuario = require( '../models/usuario' );
const Password = require( '../models/password' );
const Database = require( '../models/database' );


const global = async ( req, res ) => {

  const buscar = new RegExp( req.params.buscar, 'i' );
  // const regex = new RegExp( buscar, 'i' );
  

  const [ usuarios, database, dbpass ] = await Promise.all([
    Usuario.find({ nombre: buscar }),
    Database.find({ servicio: buscar, host: buscar }),
    Password.find({ usuario: buscar }),
  ])



  res.json({
    ok: true,
    usuarios,
    database,
    dbpass
  });

}

const usuarios = async ( req, res ) => {

  const buscar = new RegExp( req.params.buscar, 'i' );

  const usuarios = await Usuario.find( { nombre: buscar } );

  res.json({
    ok: true,
    usuarios
  });
}


module.exports = {
  global,
  usuarios
}