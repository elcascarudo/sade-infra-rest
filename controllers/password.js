//Logeo de la plicacion
const logger = require('log4js').getLogger('dbpass');

const { response } = require('express');

const Password = require( '../models/password' );


const listar = async ( req, res = response ) => {

  const relacion =  req.params.relacion;

  try {

    const dbpassDB = await Password.find( {relacion} );

    logger.info( dbpassDB );

    if ( !dbpassDB ) {
      return res.status( 404 ).json({
        ok: false,
        msg: `No existe una instancia de BBDD con ese ID`
      });
    }

    res.json({
      ok: true,
      passwords: dbpassDB
    });
    
  } catch (error) {
    logger.error( `listar - controller - ${ error }` );

    res.status( 500 ).json({
      ok: false,
      msg: 'Hable con el administrador'
    });
  }

}


const crear = async ( req, res = response ) => {

  console.log( req.uid );

  const dbpass = new Password({
    creador: req.uid,
    ...req.body
  });

  try {

    // const dbpassDB = await Password.findOne( { usuario: req.body.usuario } );

    // if ( dbpassDB ) {
    //   return res.status( 500 ).json({
    //     ok: false,
    //     msg: `El usuarios '${ req.body.usuario }' ya existe`
    //   });
    // }


    await dbpass.save();

    res.json({
      ok: true,
      password: dbpass
    });
    
  } catch (error) {
   
    logger.error( `Database password - controller - ${ error }` );

    res.status( 500 ).json({
      ok: false,
      msg: 'Hable con el administrador'
    });

  }

}


const actualizar = async ( req, res = response ) => {

  res.json({
    ok: true,
    msg: 'Actualizar password de BBDD'
  });


}


const eliminar = async ( req, res = response ) => {

  const id = req.params.id;

  await Password.findByIdAndRemove( id );

  res.json({
    ok: true,
    msg: 'Se elimino el Usuario y Contraseña correctamente'
  });


}

module.exports = {
  listar,
  crear,
  actualizar,
  eliminar,
}