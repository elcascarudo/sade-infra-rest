//Logeo de la plicacion
const logger = require('log4js').getLogger('upload');
const { v4: uuidv4 } = require('uuid');
const fs = require( 'fs' );
const path = require( 'path' );


const Usuario = require( '../models/usuario' );



const imagenPerfil = async ( req, res ) => {

  const uid = req.params.id;

  // Valido si se envio algun archivo
  if (!req.files || Object.keys(req.files).length === 0) {
    return res.status(400).json({
      ok: false,
      msh: 'No hay imagen para poder subir'
    });
  }

  // Procesar Imagen
  const file = req.files.imagen;

  const nombreCortado = file.name.split('.');
  const extencionArchio = nombreCortado[ nombreCortado.length - 1 ];
  
  const extencionesValidas = [ 'png', 'jpg', 'jpeg', 'gif', 'JPG' ];

  if ( !extencionesValidas.includes( extencionArchio ) ) {
    return res.status(400).json({
      ok: false,
      msh: 'No es una extención permitida'
    });
  }
  //Nombre de archivo unico
  const nombreArchivo = `${ uuidv4() }.${ extencionArchio }`;
  // Path para guardar imagen
  const path = `./uploads/perfil/${ nombreArchivo }`;

  file.mv( path, (err) => {

  
    if (err){
  
      logger.error( err );
      return res.status(500).json({
        ok: false,
        msh: 'Error al mover la imagen'
      });
    }
    
  });

  //---------------------------------------------------------
  //---------------------------------------------------------
  // Actualizar imagen perfil

  try {
    
    const usuarioDB = await Usuario.findById( {_id: uid} );

    
    if( fs.existsSync( `./uploads/perfil/${usuarioDB.img}` ) ){
      // Borro la imagen del path
      fs.unlinkSync( `./uploads/perfil/${usuarioDB.img}` );
    }
    // Agrego la nueva imagen
    usuarioDB.img = nombreArchivo;
    // Actualizo la BBDD
    const usrActualizado = await usuarioDB.save(); //Usuario.findByIdAndUpdate( {_id: uid}, usuarioDB, { new: true } );

    res.json({
      ok: true,
      usuario: usrActualizado
    });

  } catch (error) {

    logger.error( error );

    

    return res.status(500).json({
      ok: false,
      msh: 'Ocurrio un error'
    });
  }

  
}



/*********************************************************************************************
 * Ver fotos
 */

const verFotos = async ( req, res ) => {

  // const usuarioDB = await Usuario.findById( {_id: req.uid} );

  const imagen = req.params.imagen;

  const pathFoto = path.join( __dirname, `../uploads/perfil/${ imagen }` );

  const noImage = path.join( __dirname, `../uploads/no-image.png` );

  if( fs.existsSync( pathFoto ) ){
    // Borro la imagen del path
    res.sendFile( pathFoto );
  } else {
    res.sendFile( noImage );
  }


}





module.exports = {
  imagenPerfil,
  verFotos
}