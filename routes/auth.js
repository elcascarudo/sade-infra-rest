const { Router } = require( 'express' );
const { check } = require( 'express-validator' );


const { validarJWT } = require( '../middleware/validar-jwt' );
const { validarCampos } = require('../middleware/validar-campos');

const { login, renewToken, restorePassword } = require( '../controllers/auth' );


const router = Router();


router.post( '/',
  [ // Middleware de la ruta
    check( 'email', 'El Email es obligatorio' ).isEmail(),
    check( 'password', 'La contraseña es obligatori' ).not().isEmpty(),
    validarCampos
  ],
  login
);

router.get( '/renew', [ validarJWT ], renewToken );

router.get( '/restorePassword/:email', restorePassword );




module.exports = router;