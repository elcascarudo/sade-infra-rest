/**
 * Accesos Rapidos
 * 
 * /api/accesos/rapidos
 */
 const { Router } = require( 'express' );
 const { check } = require( 'express-validator' );
 
 const { validarCampos } = require( '../middleware/validar-campos' );
 const { validarJWT } = require( '../middleware/validar-jwt' );
 
 // Importación de los Controladores
 const { 
        listar,
        crear,
        detalle,
        eliminar
       } = require( '../controllers/accesos-rapidos' );
 
 
 const router = Router();
 
 // Rutas para los usuarios
 
 router.get( '/', validarJWT, listar );

 router.get( '/detalle/:id', validarJWT, detalle );
 
 router.post( '/', 
   [
      validarJWT,
      check( 'nombre', 'El nombre es requerido' ).not().isEmpty(),
      check( 'usuario', 'El usuarios es requerido' ).not().isEmpty(),
      check( 'password', 'La Contraseña es requerida' ).not().isEmpty(),
      check( 'url', 'La URL es requerida' ).not().isEmpty(),
      validarCampos    
   ], 
   crear 
 );
 
 // router.put( '/:id', 
 //   [], 
 //   actualizar 
 // );
 
 router.delete( '/:id', validarJWT, eliminar );
 
 
 // Exportación de las rutas
 module.exports = router;
