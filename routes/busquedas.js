/**
 * Controlador para generar busquedas
 * 
 * /api/buscar
 */
const { Router } = require( 'express' );
const { check } = require( 'express-validator' );
const { validarJWT } = require( '../middleware/validar-jwt' );

// Importación de los Controladores
const { 
      global,
      usuarios,
      } = require( '../controllers/busquedas' );


const router = Router();

router.get( '/global/:buscar', validarJWT, global );
router.get( '/usuarios/:buscar', validarJWT, usuarios );


module.exports = router;