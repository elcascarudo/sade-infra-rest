/**
 * Rutas para subir archivos
 * 
 * /api/upload
 */
 const { Router } = require( 'express' );
 const expressFileUpload = require( 'express-fileupload' );


 const { validarJWT } = require( '../middleware/validar-jwt' );
 
 // Importación de los Controladores
 const  { 
          imagenPerfil, 
          verFotos 
        } = require( '../controllers/upload' );
 


 const router = Router();
 router.use( expressFileUpload() );



 router.put( '/foto/perfil/:id', validarJWT, imagenPerfil );
 router.get( '/foto/ver/:imagen', verFotos );
 
 
 module.exports = router;