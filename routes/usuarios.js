/**
 * Ruta usuarios
 * 
 * /api/usuarios
 */


//------> Importaciones

const { Router } = require( 'express' );
const { check } = require( 'express-validator' );

const { validarCampos } = require( '../middleware/validar-campos' );
const { validarJWT } = require( '../middleware/validar-jwt' );

// Importación de los Controladores
const { 
        listar,
        crear,
        actualizar,
        eliminar
      } = require( '../controllers/usuarios' );


const router = Router();

// Rutas para los usuarios

router.get( '/', validarJWT, listar );

router.post( '/', 
  // en el segundo parametro se colocan lo Middleware correspondientes a la ruta
  [
    validarJWT,
    check( 'nombre', 'El nombre es obligatorio' ).not().isEmpty(),
    check( 'password', 'La contraseña es obligatoria' ).not().isEmpty(),
    check( 'email', 'El email es obligatorio' ).isEmail(),
    validarCampos // Middleware personalizado
  ], 
  crear 
);

router.put( '/:id', 
  // en el segundo parametro se colocan lo Middleware correspondientes a la ruta
  [
    validarJWT,
    check( 'nombre', 'El nombre es obligatorio' ).not().isEmpty(),
    check( 'email', 'El email es obligatorio' ).isEmail(),
    validarCampos // Middleware personalizado
  ], 
  actualizar 
);

router.delete( '/:id', validarJWT, eliminar );




// Exportación de las rutas
module.exports = router;