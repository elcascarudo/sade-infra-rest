/**
 * Servidores de todos los clientes
 * 
 * /api/database
 */
 const { Router } = require( 'express' );
 const { check } = require( 'express-validator' );
 
 const { validarCampos } = require( '../middleware/validar-campos' );
 const { validarJWT } = require( '../middleware/validar-jwt' );
 
 // Importación de los Controladores
 const { 
        listar,
        crear
       } = require( '../controllers/servidores-clientes' );
 
 
 const router = Router();
 
 // Rutas para los usuarios
 
 router.get( '/', validarJWT, listar );
 
 router.post( '/', 
   [
      validarJWT,
      check( 'ambiente', 'El ambiente es requerido' ).not().isEmpty(),
      check( 'entorno', 'El entorno es requerido' ).not().isEmpty(),
      check( 'desarrollo', 'El desarrollo es requerido' ).not().isEmpty(),
      check( 'modulo', 'El módulo es requerido' ).not().isEmpty(),
      //check( 'tipo', 'El host es requerido' ).not().isEmpty(),
      check( 'nodo', 'El nodo es requerido' ).not().isEmpty(),
      check( 'ip', 'La IP es requerida' ).not().isEmpty(),
      validarCampos    
   ], 
   crear 
 );
 
 // router.put( '/:id', 
 //   [], 
 //   actualizar 
 // );
 
 // router.delete( '/:id', validarJWT, eliminar );
 
 
 // Exportación de las rutas
 module.exports = router;
