/**
 * Base de Datos de todos los entornos
 * 
 * /api/database
 */
 const { Router } = require( 'express' );
 const { check } = require( 'express-validator' );
 
 const { validarCampos } = require( '../middleware/validar-campos' );
 const { validarJWT } = require( '../middleware/validar-jwt' );
 
 // Importación de los Controladores
 const { 
        listar,
        crear,
        actualizar,
        eliminar,
       } = require( '../controllers/password' );
 
 
 const router = Router();
 
 // Rutas para los usuarios
 
 router.get( '/:relacion', validarJWT, listar );
 
 router.post( '/', 
   [
    validarJWT,
    validarCampos

   ], 
   crear 
 );
 
 router.put( '/:id', 
   [
    validarJWT
   ], 
   actualizar 
 );
 
 router.delete( '/:id', validarJWT, eliminar );
 
 
 
 
 // Exportación de las rutas
 module.exports = router;


