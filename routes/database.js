/**
 * Base de Datos de todos los entornos
 * 
 * /api/database
 */
 const { Router } = require( 'express' );
 const { check } = require( 'express-validator' );
 
 const { validarCampos } = require( '../middleware/validar-campos' );
 const { validarJWT } = require( '../middleware/validar-jwt' );
 
 // Importación de los Controladores
 const { 
        listar,
        crear,
        actualizar,
        eliminar,
        detalle
       } = require( '../controllers/database' );
 
 
 const router = Router();
 
 // Rutas para los usuarios
 
 router.get( '/', validarJWT, listar );


 router.get( '/detalle/:id', validarJWT, detalle );
 
 router.post( '/', 
   [
      validarJWT,
      check( 'host', 'El host es requerido' ).not().isEmpty(),
      check( 'puerto', 'El puerto es requerido' ).not().isEmpty(),
      check( 'servicio', 'El servicio es requerido' ).not().isEmpty(),
      check( 'ambiente', 'El ambiente es requerido' ).not().isEmpty(),
      check( 'entorno', 'El entorno es requerido' ).not().isEmpty(),
      validarCampos    
   ], 
   crear 
 );
 
 router.put( '/:id', 
   [], 
   actualizar 
 );
 
 router.delete( '/:id', validarJWT, eliminar );
 
 
 
 
 // Exportación de las rutas
 module.exports = router;


