/**
 * Ruta ping
 * 
 * /api/ping
 */


//------> Importaciones

const { Router } = require( 'express' );


// Importación de los Controladores
const { 
        ping
      } = require( '../controllers/ping' );


const router = Router();

// Rutas para los usuarios

router.get( '/', ping );


// Exportación de las rutas
module.exports = router;