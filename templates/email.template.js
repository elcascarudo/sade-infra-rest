const templateRestorePassword = ( password ) => {
  return {
    title: 'LLego tu nueva contraseña',
    body: `
        Al cambiar tu contraseña no tiene tiempo de caducidad, por lo que la podes cambiar desde tu perfil cuando quieras nuevamente: <br><br> 
        Tu nueva contraseña es: <br>
        <b>Contraseña:</b> ${ password } <br><br><br>
        Saludos, gracais!`
  }
}

module.exports =  { 
                    templateRestorePassword
                   }