const { Schema, model } = require( 'mongoose' );

const passwordSchema = Schema({

  tipo: {
    type: String,
    require: true
  },
  usuario: {
    type: String,
    require: true
  },
  password: {
    type: String,
    require: true
  },
  relacion: {
    type: Schema.Types.ObjectId
  },
  creador: {
    type: Schema.Types.ObjectId,
    ref: 'Usuario'
  }

},
{
  collection: 'passwords' // Como quiero que aparesca en la bbdd la colección
});

passwordSchema.method( 'toJSON', function() {
  // Quito los campos "__v, _id, password"
  const { __v, _id, ...object } = this.toObject();
  // Cambio el _id por id y lo agrego al objeto, solo a modo visual
  object.id = _id;
  return object;
})

module.exports = model( 'Passwords', passwordSchema );