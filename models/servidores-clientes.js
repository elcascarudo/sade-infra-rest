const { Schema, model } = require( 'mongoose' );

const servidoresClientesSchema = Schema({


  ambiente: {
    type: String,
    require: true
  },
  entorno: {
    type: String,
    require: true
  },
  desarrollo: {
    type: String,
    require: true
  },
  modulo: {
    type: String,
    require: true
  },
  tipo: {
    type: String,
    require: true
  },
  nodo: {
    type: String,
    require: true
  },
  ip: {
    type: String,
    require: true
  },
  creador: {
    type: Schema.Types.ObjectId,
    ref: 'Usuario'
  }

},
{
  collection: 'servidoresclientes' // Como quiero que aparesca en la bbdd la colección
});

servidoresClientesSchema.method( 'toJSON', function() {
  // Quito los campos "__v, _id, password"
  const { __v, _id, ...object } = this.toObject();
  // Cambio el _id por id y lo agrego al objeto, solo a modo visual
  object.id = _id;
  return object;
})

module.exports = model( 'servidoresClientes', servidoresClientesSchema );