const { Schema, model } = require( 'mongoose' );

const accesosRapidosSchema = Schema({


  nombre: {
    type: String,
    require: true
  },
  usuario: {
    type: String,
    require: true
  },
  password: {
    type: String,
    require: true
  },
  url: {
    type: String,
    require: true
  },
  creador: {
    type: Schema.Types.ObjectId,
    ref: 'Usuario'
  }

},
{
  collection: 'accesosRapidos' // Como quiero que aparesca en la bbdd la colección
});

accesosRapidosSchema.method( 'toJSON', function() {
  // Quito los campos "__v, _id"
  const { __v, _id, ...object } = this.toObject();
  // Cambio el _id por id y lo agrego al objeto, solo a modo visual
  object.id = _id;
  return object;
})

module.exports = model( 'accesosRapidos', accesosRapidosSchema );