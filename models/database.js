const { Schema, model } = require( 'mongoose' );

const databaseSchema = Schema({

  host: {
    type: String,
    require: true
  },
  puerto: {
    type: Number,
    require: true
  },
  servicio: {
    type: String,
    require: true
  },
  ambiente: {
    type: String,
    require: true
  },
  entorno: {
    type: String,
    require: true
  },
  usuario: {
    require: true,
    type: Schema.Types.ObjectId,
    ref: 'Usuario'
  },
  estado: {
    type: Boolean,
    default: true
  }

},
{
  collection: 'databases' // Como quiero que aparesca en la bbdd la colección
});

databaseSchema.method( 'toJSON', function() {
  // Quito los campos "__v, _id, password"
  const { __v, _id, ...object } = this.toObject();
  // Cambio el _id por uid y lo agrego al objeto, solo a modo visual
  object.id = _id;
  return object;
})

module.exports = model( 'Database', databaseSchema );