// Levanta las variables de entorno del archivo .env
require( 'dotenv' ).config();

//Importación de terceros
const logger = require('log4js').getLogger();
const express = require( 'express' );
const cors = require('cors');


// Importación propias
const { dbConection } = require( './database/config' );

// Nivel de Debug
logger.level = process.env.DEBUG_LEVEL;

//Crear servicor Express
const app = express();

app.use( cors() );              // Cors origin
app.use( express.json() );      // Lectura y parseo del Body

// Conexión a la BBDD
dbConection();

//rutas
app.use( '/api/ping',                 require( './routes/ping' ) );

app.use( '/api/usuarios',             require( './routes/usuarios' ) );
app.use( '/api/database',             require( './routes/database' ) );
app.use( '/api/password',             require( './routes/password' ) );
app.use( '/api/buscar',               require( './routes/busquedas' ) );
app.use( '/api/upload',               require( './routes/upload' ) );
app.use( '/api/login',                require( './routes/auth' ) );
app.use( '/api/servidores/clientes',  require( './routes/servidores-clientes' ) );
app.use( '/api/accesos/rapidos',      require( './routes/accesos-rapidos' ) );


// Expongo el api-rest
app.listen( process.env.PORT, () => {
  logger.info( `Corriendo en el puerto ${ process.env.PORT }` );
});