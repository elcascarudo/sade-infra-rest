const logger = require('log4js').getLogger('enviarMail');
const nodemailer = require('nodemailer');


const enviarMail = (  email, password ) => {

  return new Promise( ( resolve, reject ) => {

    // Definimos el transporter
    const transporter = nodemailer.createTransport({
        service: 'Gmail',
        auth: {
            user: process.env.GMAIL_USS,
            pass: process.env.GMAIL_PSS
        }
    });

    // Definimos el email
    const mailOptions = {
        from: process.env.GMAIL_USS,
        to: email,
        subject: 'Nueva contraseña creada',
        //text: 'Contenido del email',
        html: `Tu nueva contraseña es: <b>${ password }</b>`, // html body
    };


    transporter.sendMail( mailOptions, ( err, info ) => {

      if( err ) {
        logger.error( err );
        reject( 'No se pudo enviar el mail' );
      } else {
        logger.debug( info );
        resolve( `Se envio una nueva contraseña al mail ${ email }` );
      }

    });

  });

};

module.exports = { enviarMail }